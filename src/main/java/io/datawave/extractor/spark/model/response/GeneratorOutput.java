package io.datawave.extractor.spark.model.response;

import io.datawave.shared.model.PipelineComponent;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeneratorOutput {

    private String logMessage;
    private List<PipelineComponent> components;
    private byte[] zipFile;
    private Map<String, Integer> counts;

    private List<String> messages;
    private List<String> warnings;
    private List<String> errors;



    public String getLogMessage() {
        return logMessage;
    }

    public void setLogMessage(String logMessage) {
        this.logMessage = logMessage;
    }

    public byte[] getZipFile() {
        return zipFile;
    }

    public void setZipFile(byte[] zipFile) {
        this.zipFile = zipFile;
    }

    public String getZipFileAsString(){
        return StringUtils.newStringUtf8(Base64.encodeBase64(this.zipFile));
    }

    public List<PipelineComponent> getComponents() {
        return components;
    }

    public void setComponents(List<PipelineComponent> components) {
        this.components = components;
    }

    public void addComponent(PipelineComponent component){
        if(this.components==null){
            this.components = new ArrayList<>();
        }

        this.components.add(component);
    }

    public Map<String, Integer> getCounts() {
        return counts;
    }

    public void setCounts(Map<String, Integer> counts) {
        this.counts = counts;
    }

    public void addCount(String label, Integer value){
        if(this.counts == null){
            this.counts = new HashMap<>();
        }

        this.counts.put(label, value);
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public void addMessage(String message){
        if(this.messages == null){
            this.messages = new ArrayList<>();
        }

        this.messages.add(message);
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<String> warnings) {
        this.warnings = warnings;
    }

    public void addWarning(String warning){
        if(this.warnings == null){
            this.warnings = new ArrayList<>();
        }

        this.warnings.add(warning);
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public void addError(String error){
        if(this.errors == null){
            this.errors = new ArrayList<>();
        }

        this.errors.add(error);
    }
}
