package io.datawave.extractor.spark.model.sparkUI;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApplicationAttempt {

    private int attemptId;
    private Date startTime;
    private Date endTime;
    private Date lastUpdated;
    private long duration;
    private String sparkUser;
    private boolean completed;
    private String appSparkVersion;
    private long startTimeEpoch;
    private long endTimeEpoch;
    private long lastUpdatedEpoch;

    public int getAttemptId() {
        return attemptId;
    }

    public void setAttemptId(int attemptId) {
        this.attemptId = attemptId;
    }

    public boolean inClusterMode(){
        return this.attemptId > 0;
    }

    public boolean inClientMode(){
        return !inClusterMode();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getSparkUser() {
        return sparkUser;
    }

    public void setSparkUser(String sparkUser) {
        this.sparkUser = sparkUser;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getAppSparkVersion() {
        return appSparkVersion;
    }

    public void setAppSparkVersion(String appSparkVersion) {
        this.appSparkVersion = appSparkVersion;
    }

    public long getStartTimeEpoch() {
        return startTimeEpoch;
    }

    public void setStartTimeEpoch(long startTimeEpoch) {
        this.startTimeEpoch = startTimeEpoch;
    }

    public long getEndTimeEpoch() {
        return endTimeEpoch;
    }

    public void setEndTimeEpoch(long endTimeEpoch) {
        this.endTimeEpoch = endTimeEpoch;
    }

    public long getLastUpdatedEpoch() {
        return lastUpdatedEpoch;
    }

    public void setLastUpdatedEpoch(long lastUpdatedEpoch) {
        this.lastUpdatedEpoch = lastUpdatedEpoch;
    }
}
