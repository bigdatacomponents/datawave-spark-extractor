package io.datawave.extractor.spark.model.sparkUI;

import com.amazon.redshift.shaded.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.datawave.extractor.spark.parsers.SparkEventParser;
import io.datawave.extractor.spark.parsers.SparkSqlStartEventParser;

import java.util.Arrays;

public enum EventType {
    SQL_EXECUTION_START("org.apache.spark.sql.execution.ui.SparkListenerSQLExecutionStart", new SparkSqlStartEventParser())
        , UNKNOWN("UNKNOWN", null);

    private String typeName;
    private SparkEventParser parser;

    EventType(String typeName, SparkEventParser parser) {
        this.typeName = typeName;
        this.parser = parser;
    }

    @JsonCreator
    public static EventType forValue(String value) {
        return Arrays.asList(EventType.values())
                    .stream()
                    .filter(type -> type.getName().equalsIgnoreCase(value))
                    .findFirst()
                    .orElse(EventType.UNKNOWN);
    }

    @JsonValue
    public String getName() {
        return typeName;
    }

    public SparkEventParser getParser() {
        return parser;
    }
}
