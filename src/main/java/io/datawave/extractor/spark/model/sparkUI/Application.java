package io.datawave.extractor.spark.model.sparkUI;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Comparator;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Application {
    private String id;
    private String name;
    private List<ApplicationAttempt> attempts;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ApplicationAttempt> getAttempts() {
        return attempts;
    }

    public void setAttempts(List<ApplicationAttempt> attempts) {
        this.attempts = attempts;
    }

    public boolean ranInClusterMode(){
        return this.attempts == null || this.attempts.size() == 0
                ? false
                : this.attempts.stream()
                    .anyMatch(attempt -> attempt.inClusterMode());
    }

    //TODO - Did not create this as negation of cluster mode due to the initial condition on null or size 0.
    public boolean ranInClientMode(){
        return this.attempts == null || this.attempts.size() == 0
                ? false
                : this.attempts.stream()
                .anyMatch(attempt -> attempt.inClientMode());
    }

    public int getLatestAttemptId(){
        return this.getAttempts().stream()
                .mapToInt(attempt -> attempt.getAttemptId())
                .max()
                .getAsInt();
    }

    public ApplicationAttempt getLatestAttempt(){
        return this.getAttempts().stream()
                .max(Comparator.comparingInt(ApplicationAttempt::getAttemptId))
                .orElse(null);
    }
}
