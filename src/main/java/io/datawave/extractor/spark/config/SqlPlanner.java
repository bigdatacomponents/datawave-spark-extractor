package io.datawave.extractor.spark.config;

import org.apache.calcite.plan.RelOptUtil;
import org.apache.calcite.plan.hep.HepPlanner;
import org.apache.calcite.plan.hep.HepProgram;
import org.apache.calcite.plan.hep.HepProgramBuilder;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rel.rules.*;
import org.apache.calcite.schema.SchemaPlus;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.parser.SqlParseException;
import org.apache.calcite.sql.parser.SqlParser;
import org.apache.calcite.tools.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class SqlPlanner {

    private static final Logger LOGGER = LoggerFactory.getLogger(SqlPlanner.class);

    private final HepPlanner rulesPlanner = buildAndGetPlannerBasedOnStandardRules(true);
    private final FrameworkConfig config;
    private final Planner planner ;

    public SqlPlanner(FrameworkConfig config) {
        this.config = config;
        this.planner = Frameworks.getPlanner(this.config);
    }

    public FrameworkConfig getConfig() {
        return config;
    }

    public SchemaPlus getDefaultSchema(){
        return this.config.getDefaultSchema();
    }

    public Optional<SqlNode> getSqlNode(String sql){

        planner.close();
        planner.reset();

        try{
            SqlNode parsedSqlNode = this.planner.parse(sql);
            return Optional.of(parsedSqlNode);
        }
        catch(SqlParseException parseException){
            LOGGER.debug("Error while parsing SQL : "+parseException.getMessage());
            return Optional.empty();
        }
    }

    public Optional<SqlNode> getSqlNodeForSqlFragment(String sql){
        SqlNode parsedSqlNode = null;
        try{
            parsedSqlNode = SqlParser.create(sql, this.config.getParserConfig()).parseExpression();
        }
        catch(SqlParseException parseException){
            parsedSqlNode = null; //Do we need this?
        }

        if(parsedSqlNode == null){
            LOGGER.info("Sql Fragment parsing did not succeed. Trying full SQL parsing...");
            return this.getSqlNode(sql);
        }
        else {
            return Optional.of(parsedSqlNode);
        }
    }

    public Optional<RelNode> getRelationalNode(String sql){

        planner.close();
        planner.reset();

        try{
            SqlNode parsedSqlNode = this.planner.parse(sql);

            try{
                SqlNode validatedSqlNode = planner.validate(parsedSqlNode);
                try {
                    //get relational node
                    RelNode root = planner.rel(validatedSqlNode).rel;

                    return Optional.of(root);

                }
                catch(RelConversionException relConversionEx){
                    LOGGER.debug("Relational Conversion Exception while converting validated SQL to RelNode : "+relConversionEx.getMessage());
                    return Optional.empty();
                }
                catch(Exception ex){
                    LOGGER.debug("Error while converting validated SQL to RelNode : "+ex.getMessage());
                    return Optional.empty();
                }
            }
            catch(ValidationException validationEx){
                LOGGER.debug("Error while validating SQL: "+validationEx.getMessage());
                return Optional.empty();
            }
        }
        catch(SqlParseException parseException){
            LOGGER.debug("Error while parsing SQL : "+parseException.getMessage());
            return Optional.empty();
        }
    }

    public Optional<RelNode> getOptimizedRelNode(String sql){

        planner.close();
        planner.reset();

        try{

            SqlNode parsedSqlNode = this.planner.parse(sql);

            try{
                SqlNode validatedSqlNode = planner.validate(parsedSqlNode);
                try {
                    //get relational node
                    RelNode root = planner.rel(validatedSqlNode).rel;

                    LOGGER.debug("=*=*=*=*=*=*=*=*=*=*=*= BEFORE OPTIMIZATION =*=*=*=*=*=*=*=*=*");
                    LOGGER.info("\n\n"+ RelOptUtil.toString(root));
                    LOGGER.debug("=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*");

                    this.rulesPlanner.setRoot(root);

                    LOGGER.info("Optimizing Relational Expression.");
                    RelNode optRelNode = this.rulesPlanner.chooseDelegate().findBestExp();

                    LOGGER.debug("=*=*=*=*=*=*=*=*=*=*=*= AFTER OPTIMIZATION =*=*=*=*=*=*=*=*=*");
                    LOGGER.info("\n\n"+RelOptUtil.toString(optRelNode));
                    LOGGER.debug("=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*");

                    return Optional.of(optRelNode);

                }
                catch(RelConversionException relConversionEx){
                    LOGGER.debug("Relational Conversion Exception while converting validated SQL to RelNode : "+relConversionEx.getMessage());
                    return Optional.empty();
                }
                catch(Exception ex){
                    LOGGER.debug("Error while converting validated SQL to RelNode : "+ex.getMessage());
                    return Optional.empty();
                }
            }
            catch(ValidationException validationEx){
                LOGGER.debug("Error while validating SQL: "+validationEx.getMessage());
                return Optional.empty();
            }
        }
        catch(SqlParseException parseException){
            LOGGER.debug("Error while parsing SQL : "+parseException.getMessage());
            return Optional.empty();
        }
    }

    public static HepPlanner buildAndGetPlannerBasedOnStandardRules(boolean optimize){
        HepProgramBuilder hepProgramBuilder = new HepProgramBuilder();

        if(optimize){
            hepProgramBuilder.addRuleInstance(UnionMergeRule.INSTANCE)
                    //.addRuleInstance(JoinToMultiJoinRule.INSTANCE)
                    //.addRuleInstance(FilterMultiJoinMergeRule.INSTANCE)
                    //.addRuleInstance(ProjectMultiJoinMergeRule.INSTANCE)
                    .addRuleInstance(ProjectRemoveRule.INSTANCE)
                    .addRuleInstance(ProjectMergeRule.INSTANCE)
            ;
        }

        HepProgram hepProgram = hepProgramBuilder.build();

        HepPlanner planner = new HepPlanner(hepProgram);

        return planner;
    }

}
