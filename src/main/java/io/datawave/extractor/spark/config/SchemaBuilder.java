package io.datawave.extractor.spark.config;

import io.datawave.extractor.spark.extensions.functions.scalar.IsAlphaNumeric;
import io.datawave.extractor.spark.extensions.functions.scalar.IsDate;
import io.datawave.extractor.spark.extensions.functions.scalar.IsEmail;
import io.datawave.extractor.spark.extensions.functions.scalar.Param;
import io.datawave.extractor.spark.schema.CatalogueSchema;
import org.apache.calcite.schema.SchemaPlus;
import org.apache.calcite.sql.SqlOperatorTable;
import org.apache.calcite.sql.parser.SqlParser;
import org.apache.calcite.sql.util.ChainedSqlOperatorTable;
import org.apache.calcite.sql.util.ListSqlOperatorTable;
import org.apache.calcite.sql.validate.SqlConformanceEnum;
import org.apache.calcite.tools.FrameworkConfig;
import org.apache.calcite.tools.Frameworks;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Configuration
public class SchemaBuilder {

    public FrameworkConfig getConfigForSchemas(List<CatalogueSchema> schemas) throws IOException {

        //build sql operator tables which includes both standard and user defined operators.
        SqlOperatorTable standardSqlOperatorTable = Frameworks.newConfigBuilder().build().getOperatorTable();
        ListSqlOperatorTable userdefinedOperators = new ListSqlOperatorTable();
        userdefinedOperators.add(new Param());
        userdefinedOperators.add(new IsDate());
        userdefinedOperators.add(new IsEmail());
        userdefinedOperators.add(new IsAlphaNumeric());
        ChainedSqlOperatorTable finalOpTable = new ChainedSqlOperatorTable(Arrays.asList(standardSqlOperatorTable, userdefinedOperators));

        SchemaPlus rootSchema = Frameworks.createRootSchema(true);

        schemas.forEach(schema -> rootSchema.add(schema.getName(), schema));

        return Frameworks.newConfigBuilder().defaultSchema(rootSchema).operatorTable(finalOpTable).build();
    }

    public FrameworkConfig getConfigForSchemasAddingAllTablesToRootSchema(List<CatalogueSchema> schemas) throws IOException {

        //build sql operator tables which includes both standard and user defined operators.
        SqlOperatorTable standardSqlOperatorTable = Frameworks.newConfigBuilder().build().getOperatorTable();
        ListSqlOperatorTable userdefinedOperators = new ListSqlOperatorTable();
        userdefinedOperators.add(new Param());
        userdefinedOperators.add(new IsDate());
        userdefinedOperators.add(new IsEmail());
        userdefinedOperators.add(new IsAlphaNumeric());
        ChainedSqlOperatorTable finalOpTable = new ChainedSqlOperatorTable(Arrays.asList(standardSqlOperatorTable, userdefinedOperators));

        SchemaPlus rootSchema = Frameworks.createRootSchema(true);

        schemas.forEach(schema -> {
            schema.getTables().forEach(table -> {
                if(rootSchema.getTable(table.getName()) == null){
                    rootSchema.add(table.getName(), table);
                }
            });
        });

        SqlParser.Config parserConfig =
                SqlParser.configBuilder()
                        .setConformance(SqlConformanceEnum.ORACLE_12)
                        .build();


        return Frameworks.newConfigBuilder()
                .defaultSchema(rootSchema)
                .parserConfig(parserConfig)
                .operatorTable(finalOpTable).build();
    }

    public FrameworkConfig getDefaultFrameworkConfig(){

        //build sql operator tables which includes both standard and user defined operators.
        SqlOperatorTable standardSqlOperatorTable = Frameworks.newConfigBuilder().build().getOperatorTable();

        SchemaPlus rootSchema = Frameworks.createRootSchema(true);

        SqlParser.Config parserConfig =
                SqlParser.configBuilder()
                        .setConformance(SqlConformanceEnum.ORACLE_12)
                        .build();

        return Frameworks.newConfigBuilder()
                .defaultSchema(rootSchema)
                .parserConfig(parserConfig)
                .operatorTable(standardSqlOperatorTable).build();
    }
}
