package io.datawave.extractor.spark.config;

import io.datawave.extractor.spark.output.model.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Configuration
public class SqlValidator {

    @Value("${datawave.sql.validator}")
    private String validationUrl;

    @Autowired
    private RestTemplate restTemplate;

    public ValidationResult validate(String token, String sql){

        //build headers
        HttpHeaders headers = getHeaders(token);

        //build request body
        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add("sql", sql);
        parameters.add("withStructures", Boolean.TRUE);

        HttpEntity<MultiValueMap<String, Object>> requestEntity =
                new HttpEntity<>(parameters, headers);

        ValidationResult validationResult =
                restTemplate.exchange(validationUrl
                , HttpMethod.POST
                , requestEntity
                , new ParameterizedTypeReference<ValidationResult>(){}).getBody();

        return validationResult;
    }

    private HttpHeaders getHeaders(String token){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+ token);
        return headers;
    }

}
