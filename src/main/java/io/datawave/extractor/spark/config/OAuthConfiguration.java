package io.datawave.extractor.spark.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;

@Configuration
@Profile("!unsecured") //Don't apply security if we are using the 'unsecured' profile
@EnableResourceServer
public class OAuthConfiguration extends ResourceServerConfigurerAdapter {

    @Autowired
    private DataSource datasource;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        TokenStore tokenStore = new JdbcTokenStore(datasource);
        resources.resourceId("datawave-resource-server")
                .tokenStore(tokenStore);
    }
}

