package io.datawave.extractor.spark.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * 
 * @author Vincent Gatehouse
 *
 *  [2017] ETL Factory Limited
 *  All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of ETL Factory Limited and its suppliers, if
 * any. The intellectual and technical concepts contained herein are proprietary to ETL Factory Limited and its
 * suppliers and may be covered by U.K. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from ETL Factory Limited.
 * 
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@ComponentScan(basePackages = {"io.datawave.extractor.spark","io.datawave.shared","io.datawave.user"})
public class WebApplication {


	// MAIN METHOD
    public static void main(String[] args) {
    	Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override public void run() {
				System.out.println("WEB APPLICATION SHUT DOWN"); } }));
    	try {
    		SpringApplication.run(WebApplication.class, args); }
    	catch (Exception e) {
    		e.printStackTrace(System.out); } }

}