package io.datawave.extractor.spark.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:spark.properties")
@ConfigurationProperties(prefix="spark")
public class SparkConfig {
    private Long componentTypeId;
    private String componentTypeName;
    private String componentTypeDescription;
    private String componentTypeShortCode;
    private String historyServerUrl;
    private String applicationLogUrl;
    private String applicationAttemptLogUrl;

    public Long getComponentTypeId() {
        return componentTypeId;
    }

    public void setComponentTypeId(Long componentTypeId) {
        this.componentTypeId = componentTypeId;
    }

    public String getComponentTypeName() {
        return componentTypeName;
    }

    public void setComponentTypeName(String componentTypeName) {
        this.componentTypeName = componentTypeName;
    }

    public String getComponentTypeDescription() {
        return componentTypeDescription;
    }

    public void setComponentTypeDescription(String componentTypeDescription) {
        this.componentTypeDescription = componentTypeDescription;
    }

    public String getComponentTypeShortCode() {
        return componentTypeShortCode;
    }

    public void setComponentTypeShortCode(String componentTypeShortCode) {
        this.componentTypeShortCode = componentTypeShortCode;
    }

    public String getHistoryServerUrl() {
        return historyServerUrl;
    }

    public void setHistoryServerUrl(String historyServerUrl) {
        this.historyServerUrl = historyServerUrl;
    }

    public String getApplicationLogUrl() {
        return applicationLogUrl;
    }

    public void setApplicationLogUrl(String applicationLogUrl) {
        this.applicationLogUrl = applicationLogUrl;
    }

    public String getApplicationAttemptLogUrl() {
        return applicationAttemptLogUrl;
    }

    public void setApplicationAttemptLogUrl(String applicationAttemptLogUrl) {
        this.applicationAttemptLogUrl = applicationAttemptLogUrl;
    }
}
