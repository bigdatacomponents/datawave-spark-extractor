package io.datawave.extractor.spark.service;

import io.datawave.extractor.spark.config.SchemaBuilder;
import io.datawave.extractor.spark.config.SparkConfig;
import io.datawave.extractor.spark.config.SqlPlanner;
import io.datawave.extractor.spark.generator.SparkExtractor;
import io.datawave.extractor.spark.model.sparkUI.Application;
import io.datawave.extractor.spark.model.sparkUI.Event;
import io.datawave.extractor.spark.output.model.ImportOutput;
import io.datawave.extractor.spark.output.model.Message;
import io.datawave.extractor.spark.output.model.enums.MessageType;
import io.datawave.extractor.spark.output.model.enums.Result;
import io.datawave.extractor.spark.schema.CatalogueSchema;
import io.datawave.extractor.spark.visitors.SqlNodeStructuralVisitor;
import org.apache.calcite.sql.SqlNode;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SparakExtractorServiceImpl implements SparakExtractorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SparakExtractorServiceImpl.class);

    @Autowired
    private SparkConfig config;

    @Autowired
    private SparkExtractor extractor;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private SchemaBuilder builder;

    @Override
    public ImportOutput getComponents(){
        if(config.getHistoryServerUrl() == null
            || config.getHistoryServerUrl().trim().length() == 0){

            String errMsg = "Unable to find base url for spark history service. Can't proceed.";
            LOGGER.error(errMsg);

            ImportOutput output = new ImportOutput();
            output.setResult(Result.FAILURE);
            output.addMessage(new Message(MessageType.ERROR, errMsg));
            return output;
        }
        else {

            List<Application> applications
                    = restTemplate.exchange(config.getHistoryServerUrl()
                    , HttpMethod.GET
                    , null
                    , new ParameterizedTypeReference<List<Application>>(){})
                    .getBody();

            String message = "Found "+applications.size()+" applications.";
            LOGGER.info(message);

            Map<Application, List<Event>> interestingEventsPerApplication = applications.stream()
                    .collect(Collectors.toMap(
                                application -> application
                                , application -> extractor.getEvents(application.getId(), application.getLatestAttemptId())));

            SqlPlanner planner = new SqlPlanner(builder.getDefaultFrameworkConfig());

            List<String> sqls = interestingEventsPerApplication.entrySet().stream()
                    .flatMap(entry -> {

                        LOGGER.info(StringUtils.repeat("*", 40));
                        LOGGER.info(String.format("Parsing "+(!entry.getKey().getLatestAttempt().isCompleted()?"un": "")+"successful Application Id [%s] with name [%s] run by [%s] has [%d] interesting events"
                                , entry.getKey().getId()
                                , entry.getKey().getName()
                                , entry.getKey().getLatestAttempt().getSparkUser()
                                , entry.getValue().size()));

                        return entry.getValue().stream()
                                .flatMap(event -> event.getType().getParser().parse(event).stream());
                    })
                    .collect(Collectors.toList());

            //process sqls with just the table names.
            sqls.stream()
                    //TODO - This is a hack. While a SQL Identifier can just be a word, sometimes it can be any text with quotes.
                    .filter(sql -> sql.matches("\\w+"))
                    .forEach(LOGGER::info);

            //process sqls that contain entire sql statements
            sqls.stream()
                    //TODO - This is a hack. While a SQL Identifier can just be a word, sometimes it can be any text with quotes.
                    .filter(sql -> !sql.matches("\\w+"))
                    .flatMap(sql -> {
                        LOGGER.info("*** SQL - "+sql);

                        Optional<SqlNode> sqlNode = planner.getSqlNode(sql);

                        if(sqlNode.isPresent()){
                            //LOGGER.info("SQL Parsing completed. Getting structures used...");
                            SqlNodeStructuralVisitor visitor = new SqlNodeStructuralVisitor();
                            sqlNode.get().accept(visitor);

                            return visitor.getAssociatedStructures().stream();
                        }
                        else {
                            LOGGER.error("Could not parse SQL - "+sql);
                            return null;
                        }
                    })
                    .forEach(structureUsed -> {
                        LOGGER.info(String.format("Structure used %s as %s", structureUsed.getName(), structureUsed.getRole().name()));
                    });
                    ;

            ImportOutput output = new ImportOutput();
            output.setResult(Result.SUCCESS);
            output.addMessage(new Message(MessageType.INFO, message));
            return output;
        }
    }
}