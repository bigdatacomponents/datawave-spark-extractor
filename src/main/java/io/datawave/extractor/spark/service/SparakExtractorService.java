package io.datawave.extractor.spark.service;

import io.datawave.extractor.spark.model.response.GeneratorOutput;
import io.datawave.extractor.spark.output.model.ImportOutput;

import java.io.InputStream;
import java.util.List;

public interface SparakExtractorService {
	ImportOutput getComponents();
}