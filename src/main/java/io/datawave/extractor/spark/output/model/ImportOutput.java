package io.datawave.extractor.spark.output.model;

import io.datawave.extractor.spark.output.model.enums.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImportOutput {

    private Result result;
    private List<Message> messages;
    private Map<String, Long> counts;
    private List<ImportedComponent> importedComponents;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<ImportedComponent> getImportedComponents() {
        return importedComponents;
    }

    public void setImportedComponents(List<ImportedComponent> importedComponents) {
        this.importedComponents = importedComponents;
    }

    public void addMessage(Message message){
        if(null == this.messages){
            this.messages = new ArrayList<>();
        }

        this.messages.add(message);
    }

    public Map<String, Long> getCounts() {
        return counts;
    }

    public void setCounts(Map<String, Long> counts) {
        this.counts = counts;
    }

    public void addCountsFor(String countType, Long count){
        if(this.counts == null){
            this.counts = new HashMap<>();
        }

        this.counts.put(countType, count);
    }
}
