package io.datawave.extractor.spark.output.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.datawave.extractor.spark.output.model.enums.Result;
import io.datawave.shared.model.Structure;

import java.util.ArrayList;
import java.util.List;


/*
 * This is similar to ValidationResult. TODO - Should have a common class definition for this (with slight diffs). May be we can use GeneratorOutput/ Inheritance used in other generators.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeneratorResult {
    private Result result;
    private List<Message> messages;
    private String input;
    private String node;
    private List<String> associatedStructureNames;
    private List<Structure> associatedStructures;
    private String sql;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void addMessages(List<Message> messages) {

        if(null == this.messages || this.messages == null || this.messages.size() == 0){
            this.messages = new ArrayList<>();
        }

        messages.forEach(message -> this.messages.add(message));
    }

    public void addMessage(Message message){
        if(null == this.messages){
            this.messages = new ArrayList<>();
        }

        this.messages.add(message);
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public List<String> getAssociatedStructureNames() {
        return associatedStructureNames;
    }

    public void setAssociatedStructureNames(List<String> associatedStructureNames) {
        this.associatedStructureNames = associatedStructureNames;
    }

    public void addAssociatedStructureName(String associatedStructureName){
        if(this.associatedStructureNames == null){
            this.associatedStructureNames = new ArrayList<>();
        }

        this.associatedStructureNames.add(associatedStructureName);
    }

    public List<Structure> getAssociatedStructures() {
        return associatedStructures;
    }

    public void setAssociatedStructures(List<Structure> associatedStructures) {
        this.associatedStructures = associatedStructures;
    }

    public void addAssociatedStructure(Structure structure){
        if(this.associatedStructures == null){
            this.associatedStructures = new ArrayList<>();
        }

        this.associatedStructures.add(structure);
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }
}

