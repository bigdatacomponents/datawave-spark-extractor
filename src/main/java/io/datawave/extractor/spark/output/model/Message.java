package io.datawave.extractor.spark.output.model;


import io.datawave.extractor.spark.output.model.enums.MessageType;

public class Message {
    private MessageType type;
    private String message;

    public Message() {
    }

    public Message(MessageType type, String message) {
        this.type = type;
        this.message = message;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static Message create(MessageType messageType, String message){
        return new Message(messageType, message);
    }
}
