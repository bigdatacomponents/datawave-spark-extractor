package io.datawave.extractor.spark.output.model.enums;

public enum Result {
    SUCCESS
    , FAILURE
    , PARTIAL;
}
