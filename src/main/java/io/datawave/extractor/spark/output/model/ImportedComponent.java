package io.datawave.extractor.spark.output.model;

import io.datawave.shared.model.PipelineComponent;

import java.util.*;

public class ImportedComponent {

    private PipelineComponent component;
    private List<StructureUsed> associatedStructures;

    public ImportedComponent() {
        this.component = null;
        this.associatedStructures = new ArrayList<>();

    }

    public ImportedComponent(PipelineComponent component, List<StructureUsed> associatedStructures) {
        this.component = component;
        this.associatedStructures = associatedStructures;
    }

    public PipelineComponent getComponent() {
        return component;
    }

    public void setComponent(PipelineComponent component) {
        this.component = component;
    }

    public List<StructureUsed> getAssociatedStructures() {
        return associatedStructures;
    }

    public void setAssociatedStructures(List<StructureUsed> associatedStructures) {
        this.associatedStructures = associatedStructures;
    }

    public void addAssociatedStructure(StructureUsed associatedStrucutre){
        if(this.associatedStructures == null){
            this.associatedStructures = new ArrayList<>();
        }

        this.associatedStructures.add(associatedStrucutre);
    }
}
