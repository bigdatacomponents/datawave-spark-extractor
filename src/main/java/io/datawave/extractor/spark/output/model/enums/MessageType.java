package io.datawave.extractor.spark.output.model.enums;

public enum MessageType {
    INFO
    , WARNING
    , ERROR
}
