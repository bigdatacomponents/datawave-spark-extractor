package io.datawave.extractor.spark.output.model.enums;

public enum StructureRole {
    SOURCE
    , TARGET;
}
