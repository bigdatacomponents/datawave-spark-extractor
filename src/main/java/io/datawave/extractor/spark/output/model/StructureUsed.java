package io.datawave.extractor.spark.output.model;

import io.datawave.extractor.spark.output.model.enums.StructureRole;

public class StructureUsed {
    private String owner;
    private String schema;
    private String name;
    private StructureRole role;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StructureRole getRole() {
        return role;
    }

    public void setRole(StructureRole role) {
        this.role = role;
    }
}
