package io.datawave.extractor.spark.web;

import io.datawave.extractor.spark.config.SqlValidator;
import io.datawave.extractor.spark.output.model.ImportOutput;
import io.datawave.extractor.spark.output.model.Message;
import io.datawave.extractor.spark.output.model.enums.MessageType;
import io.datawave.extractor.spark.output.model.enums.Result;
import io.datawave.extractor.spark.service.SparakExtractorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/extractor/spark")
public class SparkExtractorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SparkExtractorController.class);

    @Autowired
    private SparakExtractorService service;

    @Autowired
    private SqlValidator validator;

    @RequestMapping("")
    public String index() {return "Spark Extractor - Spark Extractor Service is active"; }

    @RequestMapping(value = "/components")
    public ImportOutput getComponents(){
        try{
            return service.getComponents();
        }
        catch(Exception ex){
            ImportOutput output = new ImportOutput();
            output.setResult(Result.FAILURE);
            output.addMessage(Message.create(MessageType.ERROR, ex.getMessage()));
            ex.printStackTrace();
            return output;
        }
    }
}

