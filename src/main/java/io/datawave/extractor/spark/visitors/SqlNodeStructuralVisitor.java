package io.datawave.extractor.spark.visitors;

import io.datawave.extractor.spark.output.model.StructureUsed;
import io.datawave.extractor.spark.output.model.enums.StructureRole;
import org.apache.calcite.sql.*;
import org.apache.calcite.sql.util.SqlShuttle;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SqlNodeStructuralVisitor extends SqlShuttle {

    private List<StructureUsed> associatedStructures;
    private List<String> withItemNames;

    public SqlNodeStructuralVisitor() {
        this.associatedStructures = new ArrayList<>();
        this.withItemNames = new ArrayList<>();
    }

    public List<StructureUsed> getAssociatedStructures() {
        return this.associatedStructures.stream()
                .filter(s -> !this.withItemNames.contains(s.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public SqlNode visit(SqlCall call) {

        SqlKind sqlKind = call.getKind();

        if(sqlKind == SqlKind.SELECT){
            processSelect((SqlSelect) call);
        }
//        else if(sqlKind == SqlKind.DELETE){
//
//        }
        else if(sqlKind == SqlKind.INSERT){
            processInsert((SqlInsert) call);
        }
//        else if(sqlKind == SqlKind.UPDATE){
//
//        }
        else if(sqlKind == SqlKind.JOIN){
            processJoin((SqlJoin) call);
        }
//        else if(sqlKind == SqlKind.MERGE){
//
//        }
        else if(sqlKind == SqlKind.WITH_ITEM){
            //add the name associated with item to the list, so any references to it can be excluded from final result.
            SqlWithItem sqlWithItem = (SqlWithItem) call;
            this.withItemNames.add(sqlWithItem.name.getSimple());
            super.visit(sqlWithItem);
        }
        else {
            super.visit(call);
        }

        return call;
    }

    public void processSelect(SqlSelect sqlSelect){
        SqlNode fromNode = sqlSelect.getFrom();

        if(fromNode instanceof SqlBasicCall){
            SqlBasicCall basicCall = (SqlBasicCall) fromNode;
            if(basicCall.getOperator().getKind() == SqlKind.AS
                && basicCall.getOperandList().get(0).getKind() == SqlKind.IDENTIFIER){

                getAssociatedSourceStructure((SqlIdentifier) basicCall.getOperandList().get(0));

                //visit all other child nodes of this except the from clause as we already visited it.
                SqlUtil.toNodeList(new SqlNode[]
                        {sqlSelect.getSelectList()
                        , sqlSelect.getWhere()
                        , sqlSelect.getOrderList()
                        , sqlSelect.getWindowList()
                        , sqlSelect.getFetch()
                        , sqlSelect.getHaving()
                        , sqlSelect.getGroup()})
                        .accept(this);
            }
            else {
                super.visit(sqlSelect);
            }
        }
        else if(fromNode.getKind() == SqlKind.IDENTIFIER){
            getAssociatedSourceStructure((SqlIdentifier) fromNode);

            //visit all other child nodes of this except the from clause as we already visited it.
            SqlUtil.toNodeList(new SqlNode[]
                    {sqlSelect.getSelectList()
                            , sqlSelect.getWhere()
                            , sqlSelect.getOrderList()
                            , sqlSelect.getWindowList()
                            , sqlSelect.getFetch()
                            , sqlSelect.getHaving()
                            , sqlSelect.getGroup()})
                    .accept(this);
        }
        else {
            super.visit(sqlSelect);
        }
    }

    public void processJoin(SqlJoin sqlJoin){
        SqlNode left = sqlJoin.getLeft();
        SqlNode right = sqlJoin.getRight();

        processJoinNode(left);
        processJoinNode(right);

        SqlUtil.toNodeList(new SqlNode[]{sqlJoin.getCondition()}).accept(this);
    }

    public void processJoinNode(SqlNode joinNode){
        if(joinNode instanceof SqlBasicCall) {
            SqlBasicCall basicCall = (SqlBasicCall) joinNode;
            if (basicCall.getOperator().getKind() == SqlKind.AS
                    && basicCall.getOperandList().get(0).getKind() == SqlKind.IDENTIFIER) {
                getAssociatedSourceStructure((SqlIdentifier) basicCall.getOperandList().get(0));
            }
            else {
                SqlUtil.toNodeList(new SqlNode[]{joinNode}).accept(this);
            }
        }
        else if(joinNode.getKind() == SqlKind.IDENTIFIER){
            getAssociatedSourceStructure((SqlIdentifier) joinNode);
        }
        else {
            SqlUtil.toNodeList(new SqlNode[]{joinNode}).accept(this);
        }
    }

    public void processInsert(SqlInsert insert){
        SqlNode targetTable = insert.getTargetTable();

        if(targetTable instanceof SqlBasicCall
                && ((SqlBasicCall) targetTable).getOperator().getKind() == SqlKind.AS
                &&  ((SqlBasicCall) targetTable).getOperandList().get(0).getKind() == SqlKind.IDENTIFIER){
            SqlBasicCall basicCall = (SqlBasicCall) targetTable;
            getAssociatedTargetStructure((SqlIdentifier) basicCall.getOperandList().get(0));
        }
        else if(targetTable.getKind() == SqlKind.IDENTIFIER){
            getAssociatedTargetStructure((SqlIdentifier) targetTable);
        }
        else {
            super.visit(SqlUtil.toNodeList(new SqlNode[]{targetTable}));
        }

        if(insert.getSource().getKind() == SqlKind.SELECT){
            super.visit((SqlSelect) insert.getSource());
        }

    }

    public void getAssociatedStructureWithRole(SqlIdentifier identifier, StructureRole role){
        StructureUsed associatedStructure = new StructureUsed();

        associatedStructure.setName(identifier.getSimple());
        associatedStructure.setSchema(identifier.names.size() > 1 ? identifier.names.get(identifier.names.size() - 2) : "");
        associatedStructure.setOwner(identifier.names.size() > 1 ? identifier.names.get(identifier.names.size() - 2) : "");
        associatedStructure.setRole(role);

        this.associatedStructures.add(associatedStructure);
    }

    public void getAssociatedSourceStructure(SqlIdentifier identifier){
        getAssociatedStructureWithRole(identifier, StructureRole.SOURCE);
    }

    public void getAssociatedTargetStructure(SqlIdentifier identifier){
        getAssociatedStructureWithRole(identifier, StructureRole.TARGET);
    }

}
