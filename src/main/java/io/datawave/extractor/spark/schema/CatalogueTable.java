package io.datawave.extractor.spark.schema;


import io.datawave.shared.model.ImportSourceDetail;
import io.datawave.shared.model.Structure;
import io.datawave.shared.model.StructureAttribute;
import org.apache.calcite.config.CalciteConnectionConfig;
import org.apache.calcite.rel.RelCollation;
import org.apache.calcite.rel.RelDistribution;
import org.apache.calcite.rel.RelDistributions;
import org.apache.calcite.rel.RelReferentialConstraint;
import org.apache.calcite.rel.type.RelDataType;
import org.apache.calcite.rel.type.RelDataTypeFactory;
import org.apache.calcite.schema.Schema;
import org.apache.calcite.schema.Statistic;
import org.apache.calcite.schema.Table;
import org.apache.calcite.sql.SqlCall;
import org.apache.calcite.sql.SqlNode;
import org.apache.calcite.sql.type.SqlTypeName;
import org.apache.calcite.util.ImmutableBitSet;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class CatalogueTable implements Table
{

    private static final Pattern pattern = Pattern.compile("\\w+");

    //@JsonProperty("name")
    private String id;
    private String name;
    private Structure structure;


    private List<Integer> keyColumns;
    private List<RelReferentialConstraint> referentialConstraints = new ArrayList<>();
    private List<RelCollation> collationList = new ArrayList<>();


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public Structure getStructure() {
        return structure;
    }

    public List<StructureAttribute> getColumns() {
        return structure == null || structure.getAttributes() == null || structure.getAttributes().size() == 0
                ? new ArrayList<>()
                : structure.getAttributes();
    }

    @Override
    public RelDataType getRowType(RelDataTypeFactory typeFactory) {

        RelDataTypeFactory.Builder builder = new RelDataTypeFactory.Builder(typeFactory);

        if( this.getColumns() == null || this.getColumns().size() == 0)
            return null;
        else {
            this.getColumns().stream()
                    //.forEach(c-> builder.add(c.getName(), c.getDataType().toRelDataType((JavaTypeFactory) typeFactory)));
                    .forEach(c-> {
                                    //add a field based on whether precision and scale are allowed for this Sql data type.

                                    SqlTypeName sqlTypeName = getSqlTypeNameForStructureAttributeType(c.getType());
                                    boolean allowsPrecision = sqlTypeName.allowsPrec();
                                    boolean allowsScale = sqlTypeName.allowsScale();
                                    boolean isNullable = stringToBoolean(c.getNullability());
                                    String columnName = CatalogueTable.sqlify(c.getAttributeName());
                                    int precision = c.getPrecision() == null ? 0 : c.getPrecision();
                                    int scale = c.getScale() == null ? 0 : c.getScale();

                                    //allows both ex: DECIMAL
                                    if(allowsPrecision && allowsScale){
                                        builder
                                                .add(columnName, sqlTypeName, precision, scale)
                                                .nullable(isNullable);
                                    }
                                    //allows precision/length only ex: VARCHAR
                                    else if (allowsPrecision && (!allowsScale)){
                                        builder
                                                .add(columnName, sqlTypeName, precision)
                                                .nullable(isNullable);
                                    }
                                    //else, treat data type with no precision or scale. Ex: DATE.
                                    else {
                                        builder
                                                .add(columnName, sqlTypeName)
                                                .nullable(isNullable);
                                    }
                                }
                            );
        }

        return builder.build();
    }

    public String getIdentifier(String name){
        return !name.contains("\"") ? name.toUpperCase() : name;
    }

    @Override
    public Statistic getStatistic() {
        return new Statistic() {
            @Override
            public Double getRowCount() {
                return 1d;
            }

            @Override
            public boolean isKey(ImmutableBitSet columns) {
                //TODO - as we dont use CBO, we dont need key identification as part of stats
                return columns.contains(ImmutableBitSet.of(keyColumns));
            }

            @Override
            public List<RelReferentialConstraint> getReferentialConstraints() {
                return referentialConstraints;
            }

            @Override
            public List<RelCollation> getCollations() {
                return collationList;
            }

            @Override
            public RelDistribution getDistribution() {
                return RelDistributions.BROADCAST_DISTRIBUTED;
            }
        };
    }

    @Override
    public Schema.TableType getJdbcTableType() {
        return Schema.TableType.TABLE;
    }

    @Override
    public boolean isRolledUp(String column) {
        return false;
    }

    @Override
    public boolean rolledUpColumnValidInsideAgg(String column, SqlCall call, SqlNode parent, CalciteConnectionConfig config) {
        return false;
    }

    public List<Integer> getKeyColumns() {
        return keyColumns;
    }

    public void setKeyColumns(List<Integer> keyColumns) {
        this.keyColumns = keyColumns;
    }

    public List<RelReferentialConstraint> getReferentialConstraints() {
        return referentialConstraints;
    }

    public void setReferentialConstraints(List<RelReferentialConstraint> referentialConstraints) {
        this.referentialConstraints = referentialConstraints;
    }

    public List<RelCollation> getCollationList() {
        return collationList;
    }

    public void setCollationList(List<RelCollation> collationList) {
        this.collationList = collationList;
    }

    public SqlTypeName getSqlTypeNameForStructureAttributeType(String attrType) {

        SqlTypeName result = null;

        if(attrType.equalsIgnoreCase("VARCHAR")
                || attrType.equalsIgnoreCase("VARCHAR2")) {
            result = SqlTypeName.VARCHAR;
        }
        else if(attrType.equalsIgnoreCase("NUMBER")
                || attrType.equalsIgnoreCase("NUMERIC")) {
            result = SqlTypeName.DECIMAL;
        }
        else if(attrType.equalsIgnoreCase("DATE")) {
            result = SqlTypeName.DATE;
        }
        else if(attrType.equalsIgnoreCase("TIME")) {
            result = SqlTypeName.TIME;
        }
        else if(attrType.equalsIgnoreCase("TIMESTAMP")) {
            result = SqlTypeName.TIMESTAMP;
        }
        else if(attrType.equalsIgnoreCase("INTEGER")
                || attrType.equalsIgnoreCase("INT4")
                || attrType.equalsIgnoreCase("INT")) {
            result = SqlTypeName.INTEGER;
        }
        else if(attrType.equalsIgnoreCase("INT8")
                || attrType.equalsIgnoreCase("BIGINT")) {
            result = SqlTypeName.BIGINT;
        }
        else if(attrType.equalsIgnoreCase("FLOAT")) {
            result = SqlTypeName.FLOAT;
        }
        else if(attrType.equalsIgnoreCase("DOUBLE")) {
            result = SqlTypeName.DOUBLE;
        }
        else if(attrType.equalsIgnoreCase("BOOL")
            || attrType.equalsIgnoreCase("BOOLEAN")) {
            result = SqlTypeName.BOOLEAN;
        }
        else if(attrType.equalsIgnoreCase("BYTEA")) {
            result = SqlTypeName.BINARY;
        }
        else {
            result = SqlTypeName.VARCHAR;
        }

        return result;
    }

    public boolean stringToBoolean(String val){
        return (val != null &&
                ( val.trim().isEmpty()
                || val.equalsIgnoreCase("YES")
                || val.equalsIgnoreCase("Y")
                || val.equalsIgnoreCase("T")
                || val.equalsIgnoreCase("TRUE")))
                ? true
                : false;
    }

    public static CatalogueTable getTable(Structure structure){
        CatalogueTable table = new CatalogueTable();

        table.setId(structure.getId().toString());
        String tableName = CatalogueTable.sqlify(structure.getStructureName());


        table.setName(tableName);
        table.setStructure(structure);

        return table;
    }


    private static String sqlify(String name){
        return pattern.matcher(name).matches() ? name.trim().toUpperCase() : name;
    }

    public String getSystemLabelAsIdentifier(){
        if(this.structure == null
            || this.structure.getSystem() == null
            || this.structure.getSystem().getSystemLabel() == null)
            return null;

        else
            return this.structure.getSystem().getSystemLabel().replaceAll("\\W+", "_");
    }


    //TODO - For now ok, but these should be refactored into subtypes.

    public boolean isFlatFile(){
        if(this.structure == null
                || this.structure.getSystem() == null
                || this.structure.getSystem().getSystemLabel() == null)
            return false;
        else
            return this.structure.getSystem().getSystemType().equalsIgnoreCase("FLAT_FILE");
    }

    public boolean isDatabaseTable(){
        if(this.structure == null
                || this.structure.getSystem() == null
                || this.structure.getSystem().getSystemLabel() == null)
            return false;
        else
            return this.structure.getSystem().getSystemType().equalsIgnoreCase("ORACLE")
                    || this.structure.getSystem().getSystemType().equalsIgnoreCase("POSTGRES")
                    || this.structure.getSystem().getSystemType().equalsIgnoreCase("TERADATA")
                    || this.structure.getSystem().getSystemType().equalsIgnoreCase("MYSQL")
                    || this.structure.getSystem().getSystemType().equalsIgnoreCase("NETEZZA");
    }

    public ImportSourceDetail.SupportedDatasource getSystemType(){
        if(this.structure == null
                || this.structure.getSystem() == null
                || this.structure.getSystem().getSystemLabel() == null)
            return null;
        else
            return this.structure.getSystem().getImportSourceDetail().getDatasourceType();
    }

}
