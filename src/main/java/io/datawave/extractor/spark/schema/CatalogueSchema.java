package io.datawave.extractor.spark.schema;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.datawave.shared.model.Structure;
import org.apache.calcite.jdbc.JavaTypeFactoryImpl;
import org.apache.calcite.linq4j.tree.Expression;
import org.apache.calcite.schema.Schema;
import org.apache.calcite.schema.SchemaPlus;
import org.apache.calcite.schema.SchemaVersion;
import org.apache.calcite.schema.Table;
import org.apache.calcite.schema.impl.AbstractSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CatalogueSchema extends AbstractSchema {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogueSchema.class);
    private static final JavaTypeFactoryImpl typeFactory = new JavaTypeFactoryImpl();

    private String name;
    private List<CatalogueTable> tables;
    private Map<String, org.apache.calcite.schema.Function> functions;
    public List<CatalogueTable> getTables() {
        return tables;
    }

    public void setTables(List<CatalogueTable> tables) {
        this.tables = tables;
    }

    public void addTable(CatalogueTable table){
        if(this.tables == null){
            this.tables = new ArrayList<>();
        }

        this.tables.add(table);
    }

    @Override
    public Expression getExpression(SchemaPlus parentSchema, String name) {
        return null;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Schema snapshot(SchemaVersion version) {
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Map<String, Table> getTableMap(){
        if(this.tables == null || this.tables.size() == 0) return new HashMap<>();

        return this.tables.stream()
                    .collect(Collectors.toMap(t -> t.getName(), Function.identity()));
    }

    public Map<String, org.apache.calcite.schema.Function> getFunctions() {
        return functions;
    }

    public void setFunctions(Map<String, org.apache.calcite.schema.Function> functions) {
        this.functions = functions;
    }

    @Override
    protected Multimap<String, org.apache.calcite.schema.Function> getFunctionMultimap() {

        Multimap<String, org.apache.calcite.schema.Function> functionMultimap = ArrayListMultimap.create();

        if(this.functions == null || this.functions.size() == 0) return functionMultimap;

        this.functions.forEach( (functionName, function) -> {
            functionMultimap.put(functionName, function);
        });

        return functionMultimap;
    }

    public void addFunction(String functionName, org.apache.calcite.schema.Function function){

        if(this.functions == null){
            this.functions = new HashMap<>();
        }

        this.functions.put(functionName, function);
    }

    public static List<CatalogueSchema> getSchemasFromStructures(List<Structure> structures) {
        return structures.stream().collect(Collectors.groupingBy(Structure::getSystemId)).entrySet().stream()
                .map(entry -> {
                    Long systemId = entry.getKey();
                    List<Structure> struc = entry.getValue();
                    String schemaName = struc.get(0).getSystem().getSystemLabel().replaceAll("[^a-zA-Z0-9]", "_").toUpperCase();

                    List<CatalogueTable> tables = entry.getValue().stream()
                            .map(structure -> CatalogueTable.getTable(structure))
                            .collect(Collectors.toList());

                    CatalogueSchema schema = new CatalogueSchema();
                    schema.setName(schemaName);
                    schema.setTables(tables);

                    //LOGGER.debug("-------------------------------------------------------");
                    //LOGGER.debug("Added "+schema.getTableNames().size()+" to "+schemaName);
                    //String tablesDescription = schema.getTables().stream().map(table -> table.getName()+" {"+table.getRowType(typeFactory).getFieldList().stream().map(f -> f.getName()+":"+f.getType().getSqlTypeName().getName()).collect(Collectors.joining(","))+"}").collect(Collectors.joining("\n"));
                    //LOGGER.debug("\n\n"+tablesDescription+"\n");

                    return schema;
                })
                .collect(Collectors.toList());
    }
}
