package io.datawave.extractor.spark.extensions.functions.scalar;

import org.apache.calcite.sql.SqlFunction;
import org.apache.calcite.sql.SqlFunctionCategory;
import org.apache.calcite.sql.SqlKind;
import org.apache.calcite.sql.type.OperandTypes;
import org.apache.calcite.sql.type.ReturnTypes;

public class IsAlphaNumeric extends SqlFunction {
    public IsAlphaNumeric() {
        super("IS_ALPHANUMERIC"
                , SqlKind.OTHER_FUNCTION
                , ReturnTypes.BOOLEAN
                , null
                , OperandTypes.STRING
                , SqlFunctionCategory.USER_DEFINED_SPECIFIC_FUNCTION);
    }
}
