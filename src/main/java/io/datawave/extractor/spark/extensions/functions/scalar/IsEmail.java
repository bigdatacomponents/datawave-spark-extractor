package io.datawave.extractor.spark.extensions.functions.scalar;

import org.apache.calcite.sql.SqlFunction;
import org.apache.calcite.sql.SqlFunctionCategory;
import org.apache.calcite.sql.SqlKind;
import org.apache.calcite.sql.type.OperandTypes;
import org.apache.calcite.sql.type.ReturnTypes;

public class IsEmail  extends SqlFunction {
    public IsEmail() {
        super("IS_EMAIL"
                , SqlKind.OTHER_FUNCTION
                , ReturnTypes.BOOLEAN
                , null
                , OperandTypes.STRING
                , SqlFunctionCategory.USER_DEFINED_SPECIFIC_FUNCTION);
    }
}
