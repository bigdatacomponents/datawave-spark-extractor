package io.datawave.extractor.spark.extensions.functions.scalar;

import org.apache.calcite.sql.SqlFunction;
import org.apache.calcite.sql.SqlFunctionCategory;
import org.apache.calcite.sql.SqlKind;
import org.apache.calcite.sql.type.OperandTypes;
import org.apache.calcite.sql.type.ReturnTypes;

public class Param extends SqlFunction {
    public Param() {
        super("PARAM"
                , SqlKind.OTHER_FUNCTION
                , ReturnTypes.ARG0_NULLABLE_VARYING
                , null
                , OperandTypes.STRING
                , SqlFunctionCategory.USER_DEFINED_SPECIFIC_FUNCTION);
    }


}
