package io.datawave.extractor.spark.extensions.functions.scalar;

import org.apache.calcite.sql.SqlFunction;
import org.apache.calcite.sql.SqlFunctionCategory;
import org.apache.calcite.sql.SqlKind;
import org.apache.calcite.sql.type.OperandTypes;
import org.apache.calcite.sql.type.ReturnTypes;

public class IsDate  extends SqlFunction {
    public IsDate() {
        super("IS_DATE"
                , SqlKind.OTHER_FUNCTION
                , ReturnTypes.BOOLEAN
                , null
                , OperandTypes.STRING_STRING
                , SqlFunctionCategory.USER_DEFINED_SPECIFIC_FUNCTION);
    }
}
