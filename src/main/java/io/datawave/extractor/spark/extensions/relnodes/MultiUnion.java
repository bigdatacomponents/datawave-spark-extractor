package io.datawave.extractor.spark.extensions.relnodes;

import org.apache.calcite.linq4j.Ord;
import org.apache.calcite.plan.Convention;
import org.apache.calcite.plan.RelOptCluster;
import org.apache.calcite.plan.RelTraitSet;
import org.apache.calcite.rel.AbstractRelNode;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rel.RelWriter;
import org.apache.calcite.rel.type.RelDataType;

import java.util.List;
import java.util.stream.Collectors;

public class MultiUnion  extends AbstractRelNode {

    private final List<RelNode> inputs;
    private final RelDataType rowType;
    private final List<Boolean> unionAlls;

    public MultiUnion(RelOptCluster cluster
            , RelTraitSet traitSet
            , List<RelNode> inputs
            , RelDataType rowType
            , List<Boolean> unionAlls) {
        super(cluster, traitSet);
        this.inputs = inputs;
        this.rowType = rowType;
        this.unionAlls = unionAlls;
    }

    public MultiUnion(RelOptCluster cluster
            , List<RelNode> inputs
            , RelDataType rowType
            , List<Boolean> unionAlls) {
        super(cluster, cluster.traitSetOf(Convention.NONE));
        this.inputs = inputs;
        this.rowType = rowType;
        this.unionAlls = unionAlls;
    }

    @Override public void replaceInput(int ordinalInParent, RelNode p) {
        inputs.set(ordinalInParent, p);
    }

    @Override public RelNode copy(RelTraitSet traitSet, List<RelNode> inputs) {
        assert traitSet.containsIfApplicable(Convention.NONE);
        return new MultiUnion(getCluster()
                                , inputs
                                , rowType
                                , unionAlls);
    }

    public RelDataType deriveRowType() {
        return rowType;
    }

    public List<RelNode> getInputs() {
        return inputs;
    }

    public List<Boolean> getUnionAlls() {
        return unionAlls;
    }

    boolean containsUnionAll() {
        return this.unionAlls.stream().anyMatch(unionAll -> unionAll);
    }

    public RelWriter explainTerms(RelWriter pw) {

        List<String> unionTypes = unionAlls.stream()
                .map(unionAll -> unionAll ? "UNION ALL" : "UNION")
                .collect(Collectors.toList());

        super.explainTerms(pw);
        for (Ord<RelNode> ord : Ord.zip(inputs)) {
            pw.input("input#" + ord.i, ord.e);
        }
        return pw.item("unionTypes", unionTypes);
    }
}
