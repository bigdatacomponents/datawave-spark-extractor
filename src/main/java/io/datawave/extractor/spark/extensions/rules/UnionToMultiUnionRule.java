package io.datawave.extractor.spark.extensions.rules;

import io.datawave.extractor.spark.extensions.relnodes.MultiUnion;
import org.apache.calcite.plan.RelOptRule;
import org.apache.calcite.plan.RelOptRuleCall;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rel.core.RelFactories;
import org.apache.calcite.rel.core.Union;
import org.apache.calcite.rel.logical.LogicalUnion;
import org.apache.calcite.rel.type.RelDataType;
import org.apache.calcite.tools.RelBuilderFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UnionToMultiUnionRule extends RelOptRule{

    public static final UnionToMultiUnionRule INSTANCE =
            new UnionToMultiUnionRule(LogicalUnion.class, RelFactories.LOGICAL_BUILDER);

    public UnionToMultiUnionRule(Class<? extends Union> clazz,
                                 RelBuilderFactory relBuilderFactory) {
        super(
                operand(clazz,
                        operand(RelNode.class, any()),
                        operand(RelNode.class, any())),
                relBuilderFactory, null);
    }

    @Override
    public void onMatch(RelOptRuleCall call) {
        Union origUnion = call.rel(0);

        RelNode left = call.rel(1);
        RelNode right = call.rel(2);

        List<RelNode> newInputs = combineInputs(left, right);
        RelDataType newRowType = origUnion.getRowType();
        List<Boolean> newUnionAlls = combineUnionAlls(origUnion, left, right);

        MultiUnion multiUnion = new MultiUnion(
                                        origUnion.getCluster()
                                        , newInputs
                                        , newRowType
                                        , newUnionAlls
                                    );

        call.transformTo(multiUnion);

    }

    private List<RelNode> combineInputs(
            RelNode left
            , RelNode right
    ){
        return Stream.concat(left.getInputs().stream(), right.getInputs().stream())
                .collect(Collectors.toList());
    }

    private List<Boolean> combineUnionAlls(
            Union origUnion
            , RelNode left
            , RelNode right
    ){

        List<Boolean> newUnionAlls = left instanceof MultiUnion
                ? ((MultiUnion) left).getUnionAlls()
                : new ArrayList<>();

        newUnionAlls.add(origUnion.all);

        newUnionAlls.addAll(right instanceof MultiUnion
                ? ((MultiUnion) right).getUnionAlls()
                : new ArrayList<>());

        return newUnionAlls;
    }

    private boolean canCombine(RelNode input) {
        return input instanceof MultiUnion;
    }
}
