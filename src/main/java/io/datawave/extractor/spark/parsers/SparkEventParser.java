package io.datawave.extractor.spark.parsers;

import io.datawave.extractor.spark.model.sparkUI.Event;

import java.util.List;

public interface SparkEventParser {
    List<String> parse(Event event);
}
