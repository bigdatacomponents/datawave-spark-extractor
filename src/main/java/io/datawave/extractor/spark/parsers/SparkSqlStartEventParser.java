package io.datawave.extractor.spark.parsers;

import io.datawave.extractor.spark.model.sparkUI.Event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SparkSqlStartEventParser implements SparkEventParser {

    Pattern logicalPlanPattern = Pattern.compile(".*(== Parsed Logical Plan ==)(?<logicalPlan>.*)(== Analyzed Logical Plan ==).*", Pattern.DOTALL);
    Pattern jdbcRelationPattern = Pattern.compile(".*(JDBCRelation\\()(?<sql>.*)(\\).*)", Pattern.DOTALL);
    Pattern innerQuery = Pattern.compile("\\((?<innerSql>.*)\\).*", Pattern.DOTALL);


    @Override
    public List<String> parse(Event event) {

        List<String> plan = new ArrayList<>();

        if(event.getPlan() == null
            || event.getPlan().trim().length() == 0)
            return plan;
        else {
            Matcher matcher = logicalPlanPattern.matcher(event.getPlan());

            List<String> sqls = new ArrayList<>();

            while(matcher.find()){

                sqls.addAll(Arrays.asList(matcher.group("logicalPlan").split("\\n"))
                                        .stream()
                                        .flatMap(planLine -> {
                                            Matcher jdbcMatcher = jdbcRelationPattern.matcher(planLine);
                                            List<String> jdbcRelations = new ArrayList<>();
                                            while(jdbcMatcher.find()){
                                                //TODO -  A sql of the form "(select ....) as t", is not a valid SQL (atleast calcite thinks so). for now get the inner SQL.
                                                String sql = jdbcMatcher.group("sql");

                                                Matcher innerQueryMatcher = innerQuery.matcher(sql);

                                                if(innerQueryMatcher.find())
                                                    jdbcRelations.add(innerQueryMatcher.group("innerSql"));
                                                else
                                                    jdbcRelations.add(jdbcMatcher.group("sql"));
                                            }
                                            return jdbcRelations.stream();
                                        })
                                        .collect(Collectors.toList()));
            }

            return sqls;
        }
    }
}
