package io.datawave.extractor.spark.generator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.thoughtworks.xstream.XStream;
import io.datawave.extractor.spark.config.SparkConfig;
import io.datawave.extractor.spark.model.sparkUI.Event;
import io.datawave.extractor.spark.model.sparkUI.EventType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UriTemplate;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Service
public class SparkExtractor {

    private static final Logger LOGGER = LogManager.getLogger(SparkExtractor.class);

    @Autowired
    private SparkConfig config;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper mapper;

    public List<Event> getEvents(@NotNull String appId, int attemptId){
        URI uri = getUrl(appId, attemptId);
        Optional<Path> zipFilePath = getByteStream(uri);

        if(zipFilePath.isPresent()){
            return getEvents(zipFilePath.get());
        }
        else{
            LOGGER.warn("Could not get zip file from ["+uri.toString());
            return new ArrayList<>();
        }
    }

    private List<Event> getEvents(Path path){

        try(FileSystem zipFileSystem = FileSystems.newFileSystem(path, ClassLoader.getSystemClassLoader())){

            return Lists.newArrayList(zipFileSystem.getRootDirectories()).stream()
                    .flatMap(rootPath -> {
                        try{
                            return Files.list(rootPath);
                        }
                        catch (IOException ioEx){
                            LOGGER.error("Could not read files in "+rootPath.toString());
                            return null;
                        }
                    })
                    .map(filePath -> {
                        try{
                            return Files.readAllLines(filePath, StandardCharsets.UTF_8)
                                    .stream()
                                    .map(eventEntry -> {
                                        try {
                                            return mapper.readValue(eventEntry, Event.class);
                                        }
                                        catch(Exception ex){
                                            LOGGER.error("Error mapping event entry "+ex.getMessage());
                                            return null;
                                        }
                                    })
                                    .filter(event -> event != null && ! (event.getType()== EventType.UNKNOWN))
                                    .collect(Collectors.toList());
                        }
                        catch(IOException ioEx){
                            LOGGER.error("Error reading zip file ["+path.toString()+"] with error message - "+ioEx.getMessage());
                            return new ArrayList<Event>();
                        }
                    })
                    .flatMap(events -> events.stream())
                    .collect(Collectors.toList());

        }
        catch (IOException ioException){
            LOGGER.error("Unable to read from zip file - "+ioException.getMessage());
            ioException.printStackTrace();
            return new ArrayList<>();
        }
    }

    private URI getUrl(@NotNull String appId, int attemptId){
        Map<String, String> parameters = new HashMap<>();
        parameters.put("appId", appId);

        URI applicationLogUrl = null;

        if(attemptId > 0){
            parameters.put("attemptId", String.valueOf(attemptId));
            applicationLogUrl = UriComponentsBuilder.fromUriString(config.getApplicationAttemptLogUrl())
                    .buildAndExpand(parameters).toUri();

        }
        else {
            applicationLogUrl = UriComponentsBuilder.fromUriString(config.getApplicationLogUrl())
                    .buildAndExpand(parameters).toUri();
        }

        return applicationLogUrl;
    }

    private Optional<Path> getByteStream(URI uri){
        ResponseExtractor<Optional<Path>> responseExtractor =
                response -> {
                    if(response.getStatusCode().is2xxSuccessful()){
                        Path zipFilePath = Files.createTempFile("app-", "-log.zip");
                        Files.copy(response.getBody(), zipFilePath, StandardCopyOption.REPLACE_EXISTING);
                        zipFilePath.toFile().deleteOnExit();
                        return Optional.of(zipFilePath);
                    }
                    else return Optional.empty();
                };

        return this.restTemplate.execute(uri, HttpMethod.GET, null, responseExtractor);
    }

}
