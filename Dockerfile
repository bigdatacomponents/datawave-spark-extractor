FROM openjdk:8-jdk-alpine
RUN apk update;
RUN apk add bash;
RUN apk --no-cache add ca-certificates && update-ca-certificates
RUN apk add git;
RUN apk add openssh
RUN mkdir /tmp/build; cd /tmp/build
RUN mkdir ~/.ssh; touch ~/.ssh/known_hosts
RUN ssh-keyscan -t rsa bitbucket.org >> ~/.ssh/known_hosts
RUN mkdir /tmp/build/proj
COPY . /tmp/build/proj
RUN mv /tmp/build/proj/ssl/* ~/.ssh/.;
RUN chmod 400 ~/.ssh/id_rsa
RUN ssh-agent /bin/sh -c 'ssh-add ~/.ssh/id_rsa; cd /tmp/build/;git clone git@bitbucket.org:dtwv/datawave-shared.git'

RUN cd /tmp/build/datawave-shared; git checkout develop;
RUN cd /tmp/build/proj; bash gradlew build;

VOLUME /tmp

RUN mv /tmp/build/proj/build/libs/executor-java-0.0.1.jar /app.jar
ENTRYPOINT ["java","-Dspring.profiles.active=unsecured","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]